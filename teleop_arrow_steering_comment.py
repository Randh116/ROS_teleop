#!/usr/bin/env python

from __future__ import print_function

import select
import sys
import termios
import threading
import tty

import rospy
from geometry_msgs.msg import Twist


message = """
    w, a, s, d : moving keys
    i/o : increase/decrease linear speed by 10%
    k/l : increase/decrease angular speed by 10%
    any other button : stop
    
    ctrl-c : quit
"""

# pierwszy element to wartosc podawana na predkosc liniowa (linear)
# drugi na predkosc kątową (angular)
moveKeys = {
    'w': (1, 0),
    'a': (0, 1),
    's': (-1, 0),
    'd': (0, -1)
}

# mnożniki poszczególnych predkosci
# albo zwiekszaja ja o 10%, albo zmniejszaja o 10%
speedKeys = {
    'i': (1.1, 1),
    'o': (0.9, 1),
    'k': (1, 1.1),
    'l': (1, 0.9)
}

# klasa dziedziczy z threading.Thread
class ThreadTeleOp(threading.Thread):
    def __init__(self):
        # nadpisuje konstruktor z threading.Thread
        # zmusza do uzycia ponizszego konstruktora zdefiniowanego przez nas
        super(ThreadTeleOp, self).__init__()

        self.freq = 10
        self.speed_x = 0.0
        self.speed_z = 0.0
        self.rate = rospy.Rate(self.freq)
        self.x = 0.0
        self.z = 0.0
        self.condition = threading.Condition()
        self.done = False

        self.twist_publisher = rospy.Publisher('cmd_vel', Twist, queue_size = 1)

        if self.rate != 0.0:
            self.timeout = 1.0 / self.rate
        else:
            self.timeout = None

        # wystartowanie wątku
        self.start()

    def wait_for_subscribers(self):
        i = 0
        while not rospy.is_shutdown() and self.twist_publisher.get_num_connections() == 0:
            if i == 4:
                print("Waiting for subscriber to connect to {}.".format(self.twist_publisher.name))
            rospy.sleep(0.5)
            i += 1
            i = i % 5

        if rospy.is_shutdown():
            raise Exception("Shutdown request before subscribers connected.")

    def update(self, x, z, speed_x, speed_z):
        # PROGRAMOWANIE WIELOWATKOWE
        # zablokuj wątek (acquire lock) za pomoca metody z obiektu threading.Condition()
        self.condition.acquire()
        # zaktualizuj parametry
        self.x = x
        self.z = z
        self.speed_x = speed_x
        self.speed_z = speed_z
        # obudz wątek czekający na spełnienie warunku (który wymusiliśmy za pomoca .acquire)
        # i powiadom go o nowej wiadomosci, aktualizacji
        self.condition.notify()
        # zwolnij blokade (release lock)
        self.condition.release()

    def stop(self):
        self.done = True
        self.update(0, 0, 0, 0)
        # threading.join()
        # glowny watek (main) poczeka na zakonczenie tego watku (child)
        # dzieki temu nie zakonczy dzialania dopoki ten nie skonczy
        self.join()

    def run(self):
        msg = Twist()

        while not self.done:
            self.condition.acquire()
            # poczekaj na nowa wiadomosc albo na timeout
            self.condition.wait(self.timeout)

            # skopiuj parametry obiektu do wiadomosci Twist
            msg.linear.x = self.x * self.speed_x
            msg.angular.z = self.z * self.speed_z

            self.condition.release()

            # publikuj wiadomosc do topicu
            self.twist_publisher.publish(msg)

        # gdy wątek sie skonczy, publikuj 'stop', czyli pusta wiadomosc
        msg.linear.x = 0
        msg.angular.z = 0
        self.twist_publisher.publish(msg)




def getKey(key_timeout):
    # pobranie pojedynczego znaku od uzytkownika
    # tty - TeleTYpe - zestaw komend do konfiguracji terminalu
    # ustawienie terminala w tryb setraw
    tty.setraw(sys.stdin.fileno())

    # select.select(rlist, wlist, xlist[, timeout])
    # ogolnie to jest oczekiwanie na wejscie/wyjscie
    # zwraca trzy listy: rlist, wlist, xlist
    # (read-list, write-list, exception-list)
    # pierwsza lista zwraca kiedy jest gotowy do odczytu
    # druga lista kiedy jest gotowy do zapisu
    # trzecia lista kiedy występuje wyjątek
    # pozostałe dwie pozostawiamy puste bo są nam niepotrzebne
    rlist, _, _ = select.select([sys.stdin], [], [], key_timeout)

    # jesli jest gotowy do zapisu
    # odczytaj key z klawiatury
    # sys.stdin.read - odczytuje z domyslnego uchwytu do pliku, uchwytu '0'
    # domyslnie uchwyt '0' jest ustawiony jako konsola, terminal
    # odczytuje wiec sygnal z klawiatury ktory wpisujemy do terminala
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''

    # (fd, when, attributes)
    # (file descriptor, warunek, atrybuty dla pliku)
    # termios.TCSADRAIN - zmiana atrybutow nastepuje po transmisji całej kolejki
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)

    return key


# sluzy do wyswietlania aktualnych parametrow podawanych na teleop
def getVelocity(speed_x, speed_z):
    return "currently: \t speed_x %s \t speed_z %s " % (speed_x, speed_z)


if __name__ == "__main__":
    # POSIX - Portable Operating System Interface
    # przenosny interfejs dla systemu operacyjnego UNIX
    # bierze argument fd - file descriptor
    # tutaj podajemy mu domyslne wejscie z konsoli
    # pobieramy zestaw ustawien dla pliku wejsciowego
    settings = termios.tcgetattr(sys.stdin)

    # inicjalizacja nowego node'u o nazwie teleop_arrow_steering
    rospy.init_node('teleop_arrow_steering')

    # pobieranie parametrow z konsoli
    # mozemy uruchomic program z wlasnymi np.
    # rosrun teleop_arrow_steering teleop_arrow_steering.py _speed_x:=0.9 _speed_z:=0.8
    speed_x = rospy.get_param("~speed_x", 0.07)
    speed_z = rospy.get_param("~speed_z", 0.5)
    # uzywamy 'wbudowanego' parametru, sami go sobie zdefiniowalismy w obiekcie
    # mozna ewentualnie pozniej zmienic parametr 'rate', zobaczymy jak bedzie dzialac
    # repeat_rate = rospy.get_param("~repeat_rate", 0.0)
    key_timeout = rospy.get_param("~key_timeout", 0.0)
    if key_timeout == 0.0:
        key_timeout = None

    thread_teleop = ThreadTeleOp()

    x = 0
    z = 0
    status = 0

    try:
        thread_teleop.wait_for_subscribers()
        thread_teleop.update(x, z, speed_x, speed_z)

        print(message)
        print(getVelocity(speed_x, speed_z))

        while(1):
            key = getKey(key_timeout)
            if key in moveKeys.keys():
                x = moveKeys[key][0]
                z = moveKeys[key][1]
            elif key in speedKeys.keys():
                speed_x = speed_x * speedKeys[key][0]
                speed_z = speed_z * speedKeys[key][1]

                print(getVelocity(speed_x, speed_z))
                if (status == 14):
                    print(message)
                status = (status + 1) % 15
            else:
                # przestan publikowac Twist'a, jesli byl key_timeout i robot sie zatrzymal
                if key == '' and x == 0 and z == 0:
                    continue
                x = 0
                z = 0
                if (key == '\x03'):
                    break

            thread_teleop.update(x, z, speed_x, speed_z)

    except Exception as e:
        print(e)

    finally:
        thread_teleop.stop()

        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)




