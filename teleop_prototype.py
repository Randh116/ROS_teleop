#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import String
from rospy.client import init_node

class TeleOp():
    def __init__(self):
        rospy.on_shutdown(self.shutdown)
        self.freq = 10
        self.speed_x = 0.07
        self.speed_z = 0.5
        self.rate = rospy.Rate(self.freq)
        self.x = 0.0
        self.z = 0.0
        self.keys = {
            'w':[self.speed_x, 0],
            'a':[0, self.speed_z],
            's':[0,0],
            'd':[0, -self.speed_z],
            'c':[-self.speed_x, 0]
        }

        self.twist_pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
        self.teleop_sub = rospy.Subscriber('keyboard', String, self.keyboard_cb)

    def keyboard_cb(self, msg):
        if msg.data in self.keys:
            self.x = self.keys[msg.data][0]
            self.z = self.keys[msg.data][1]

    def loop(self):
        while not rospy.is_shutdown():
            msg = Twist()
            msg.linear.x = self.x
            msg.angular.z = self.z
            self.drive(msg)
            self.rate.sleep()

    def drive(self, msg):
        self.twist_pub.publish(msg)

    def shutdown(self):
        self.twist_pub.publish(Twist())

if __name__ == '__main__':
    try:
        init_node("teleop", log_level=rospy.INFO)
        t=TeleOp()
        t.loop()
    except rospy.ROSInterruptException:
        print("Bye-bye...")